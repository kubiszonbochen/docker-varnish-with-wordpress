#import the `dynamic` VMOD
vcl 4.0;
import dynamic;
backend default {
   # The `.host` parameter is not very relevant here as we'll use the dynamic
   # director as a DNS resolver for that domain.
 .host = "";
 .port = "8080";
   }
         # @see https://github.com/nigoroll/libvmod-dynamic/blob/master/src/vmod_dynamic.vcc#L237
 sub vcl_init {
    new ddir = dynamic.director(
      port = "8080",
    # The DNS resolution is done in the background,
  # see https://github.com/nigoroll/libvmod-dynamic/blob/master/src/vmod_dynamic.vcc#L48
        ttl = 10s,
   );
   }
    sub vcl_recv {
    set req.backend_hint = ddir.backend("wp-front");
                               }
