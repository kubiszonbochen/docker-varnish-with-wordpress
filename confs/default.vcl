vcl 4.0;
import dynamic;

backend default {
	.host = "";
	.port = "8080";
}

sub vcl_init {
	new ddir = dynamic.director(
	port = "8080",
	ttl = 10s,
	);
}
    
sub vcl_recv {
    set req.backend_hint = ddir.backend("wp-front");
    # cache only GET and HEAD requests
    if (req.method != "GET" && req.method != "HEAD") {
        return (pass);
    }
    return (hash);
}


